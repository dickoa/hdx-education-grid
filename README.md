## Education data availability in Africa and Middle East Heatmap
Heat map of data availability for 60 countries from Africa and Middle East on 20 indicators. 
The objective of the map is to visualize what indicators contain data and what don't at different levels of disaggregation.

<!-- ![Screen grab.](screengrab.png "Screen-grab") -->
