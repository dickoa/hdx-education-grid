function makeHeatmap(d, div_id, max, color_index) {

  color_range = [{ // white --> green
    min: '#ffffff',
    max: '#16a085'
  }, { // grey --> black
    min: '#bdc3c7',
    max: '#000000'

  }];

  console.log("The color index is: " + color_index);
  if (color_index) {
    var color_set = color_range[color_index];
  } else {
    var color_set = color_range[0];
  };

  d3.json(d, function(err, json) {
    if (err) return (err);

    else {
      countries = Object.getOwnPropertyNames(json);
      countries = countries.splice(1, countries.length);

      // Reshaping data.
      // [a, b, c]
      // a = country
      // b = indicator
      // c = assessment value
      var d = [];
      var ind_index = [];
      for (var i = 0; i != 119; ++i) ind_index.push(i)

      for (i = 0; i < countries.length; i++) {
        c = i;
        v = json[countries[i]];
        for (j = 0; j != 119; j++) {
          it = [c, ind_index[j], v[j]];
          d.push(it);
        }
      };

      console.log("Number of indicators: " + json["Indicator"].length);
      // console.log(d);

      var indicators = [];

      function cut(data) {
        indicators.push(data.substring(0, 14) + ' (...)');
      }
      json["Indicator"].forEach(cut);

      $(div_id).highcharts({

        chart: {
          type: 'heatmap',
          marginTop: 150,
          marginBottom: 80,
          marginLeft: 0,
          marginRight: 200,
          height: 2000,
          width: 800,
          reflow: true
        },

        color: ["#bae4b3", "#ecf0f1"],
        credits: false,

        title: {
          text: null
        },

        // load categories
        xAxis: {
          categories: countries,
          opposite: true
        },

        // load country list
        yAxis: {
          title: null,
          categories: json["Indicator"],
          title: null,
          opposite: true,
          minPadding: 0,
          maxPadding: 0,
          startOnTick: false,
          endOnTick: false,
          min: 0,
          offset: 0,
          maxPadding: .4,
          max: json["Indicator"].length - 1
        },

        colorAxis: {
          dataClasses: [{
            from: 0,
            to: 0,
            color: "#ecf0f1",
            name: "No data"   
          }, {
            from: 1,
            to: 2,
            color: "#bae4b3",
            name: "Available"   
          }            ]
        },
 
        legend: {
          enabled: true,
          align: 'left',
          layout: 'horizontal',
          verticalAlign: 'top',
          margin: 1000,
          y: 30,
          symbolHeight: 20,
          style: {
            "color": "contrast",
            "fontSize": "10px",
            "fontWeight": "bold",
            "textShadow": null
          }
        },

        tooltip: {
          enabled: false,
          formatter: function() {
            return '<b>' + this.point.value + '</b>' + '</b>% of sub-national data is available for <br><i>' + '<b>' + this.series.xAxis.categories[this.point.x] + '</b> on the indicator <br><b>' +
              '<i>' + this.series.yAxis.categories[this.point.y] + '</i>';
          }
        },

        series: [{
          name: 'Data Completeness',
          borderWidth: 5,
          borderRadius: 10,
          borderColor: '#ffffff',
          data: d,
          colsize: 1,
          dataLabels: {
            enabled: false,
            color: '#000000'
          }
        }]

      });
    };
  });
};

// makeHeatmap("data/indicator_data_categorical_assessment.json", '#heatmap', 4, 0);
makeHeatmap("data/data_grid3.json", '#heatmap', 4, 0);
